export const LOGIN = 'LOGIN';
export const LOADING = 'LOADING';
export const SUCCESS = 'SUCCESS';
export const FAILURE = 'FAILURE';
export const CACHE = 'CACHE';