import axios from 'axios';

const payload = (response, error) => {
    if (error) {
        return {
            payload: {},
            success: false,
            error_message: error
        }
    } else {
        return {
            payload: response,
            success: true,
            error_message: ''
        }
    }
};

export const callAPI = (url, method, params) => {
    switch (method) {
        case 'GET': {
            return axios
                .get(`${url}`, { params })
                .then(({ data }) => payload(data))
                .catch(error => payload(error, true))
        }
        default: {
            return axios
                .get(`${BFF_URL}${url}`, { params })
                .then(({ data }) => payload(data))
                .catch(error => payload(error, true))
        }
    }
}

export default callAPI;