import React, { useReducer, createContext } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import reducer, { intialState } from './reducer';
import applyMiddleware from './middleware';
import { loading, success, failure } from './actions';

export const Context = createContext(intialState);

const mapDispatchToProps = dispatch => ({
    failure: () => dispatch(failure()),
    success: data => dispatch(success(data)),
    loading: () => dispatch(loading())
});


export const StoreProvider = Component => (props) => {
    const [state, dispatch] = useReducer(reducer, { ...intialState, ...props });
    const callActions = mapDispatchToProps(dispatch);
    const callMiddleware = applyMiddleware(dispatch);

    const actions = { ...callActions, ...callMiddleware };
    return <Context.Provider value={{ state, actions }}>
        <NavigationContainer>
            <Component {...props} />
        </NavigationContainer>
    </Context.Provider>
};