import axios from 'axios';
import {AsyncStorage} from 'react-native';
import {loading, success, cache } from './actions';
import {callAPI} from "./utils/callAPI";



const lunoBTC = () => callAPI('https://api.mybitx.com/api/1/ticker', 'GET', {pair: 'XBTZAR'})
const lunoETH = () => callAPI('https://api.mybitx.com/api/1/ticker', 'GET', {pair: 'ETHZAR'})
const lunoXRP = () => callAPI('https://api.mybitx.com/api/1/ticker', 'GET', {pair: 'XRPZAR'})

const krakenBTC = () => callAPI('https://api.kraken.com/0/public/Ticker', 'GET', {pair: 'BTCEUR'})
const krakenETH = () => callAPI('https://api.kraken.com/0/public/Ticker', 'GET', {pair: 'ETHEUR'})
const krakenXRP = () => callAPI('https://api.kraken.com/0/public/Ticker', 'GET', {pair: 'XRPEUR'})

const retrieveData = (dispatch) => {
    try {
      AsyncStorage.getItem('inputAmounts').then( data => {
        if (data !== null) {
            dispatch(cache(JSON.parse(data)))
        }
      }
      );
      
    } catch (error) {
      // Error retrieving data
    }
  };

const storeData = (data, dispatch) => {
    try {
      AsyncStorage.setItem('inputAmounts', JSON.stringify(data))//.then(x => dispatch());
    } catch (error) {
      // Error saving data
    }
  }

const applyMiddleware = dispatch => ({
    getPairs: () => {
        dispatch(loading());
        return axios.all([lunoBTC(), lunoETH(), lunoXRP(), krakenBTC(), krakenETH(), krakenXRP()]).then(
            axios.spread((lunoBTCResponse, lunoETHResponse, lunoXRPResponse, krakenBTCResponse, krakenETHResponse, krakenXRPResponse) => {
                const payload = {
                    lunoBTC: lunoBTCResponse.payload,
                    lunoETH: lunoETHResponse.payload,
                    lunoXRP: lunoXRPResponse.payload,
                    krakenBTC: krakenBTCResponse.payload,
                    krakenETH: krakenETHResponse.payload,
                    krakenXRP: krakenXRPResponse.payload
                }
                return dispatch(success(payload))
            })
        )
    },
    setStorage: (data) => {
        storeData(data, dispatch)
    },
    getStorage: () => {
        retrieveData(dispatch)
    }
})

export default applyMiddleware;