import { LOGIN, LOADING, SUCCESS, FAILURE, CACHE } from './constants';

// export const onSubmit = (email, password) => ({
//     type: LOGIN,
//     payload: { email, password }
// });

export const loading = () => ({
    type: LOADING,
    payload: {}
});

export const success = data => ({
    type: SUCCESS,
    payload: { data }
})

export const failure = () => ({
    type: FAILURE,
    payload: {}
})

export const cache = (data) => ({
    type: CACHE,
    payload: { data }
})