// import 'react-native-gesture-handler';
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Home from './component';

const Stack = createStackNavigator();

const AppContainer = props => {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen name="Home" component={Home}/>
      {/* <Stack.Screen name="Settings" component={Home}/>
      <Stack.Screen name="About" component={Home}/> */}
    </Stack.Navigator>
  )
}

export default AppContainer; 