/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {useEffect, useContext, useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  StatusBar,
  TextInput,
  ActivityIndicator,
  Button,
} from 'react-native';

import _ from 'lodash';

import {Context} from '../store';

const App = props => {
  const {state, actions} = useContext(Context);
  
  const [exchangeRate, setExchangeRate] = useState(state.cache.exchangeRate || 0);
  const [bankFees, setBankFees] = useState(state.cache.bankFees || 0);
  const [amount, setAmount] = useState(state.cache.amount || 0);
  const [profitOrLoss, setProfitOrLoss] = useState([]);
  console.log("Exchange Rate", exchangeRate)
  useEffect(() => {
    actions.getStorage();
    actions.getPairs();
  }, []);
  
  const retry = () => {
    actions.getPairs();
    calculateArbitrage();
  }

  const calculateArbitrage = () => {
    const amountParam = _.isEmpty(amount) ? state.cache.amount : amount;
    const bankFeesParam = _.isEmpty(bankFees) ?  state.cache.bankFees : bankFees;
    const exchangeRateParam = _.isEmpty(exchangeRate) ?  state.cache.exchangeRate : exchangeRate;

    const krakenMaker = 0.016;
    const lunoTaker = 0.01;
    const euroInKraken = amountParam / exchangeRateParam;
    const krakenFees = (euroInKraken * krakenMaker) / 100;
    const sendingFee = 0.005;
    const withdrawalAmount = 8.5;

    const results = _.map(state.data.kraken, pair => {
      const krakenCryptoAmount = (euroInKraken - krakenFees) / pair.ask;
      const amountInLuno = krakenCryptoAmount - sendingFee;
      const findLuno = _.find(state.data.luno, x => x.coin == pair.coin);
      const saleInLuno = amountInLuno * findLuno.bid;
      const netSaleInLuno = saleInLuno * (1 - lunoTaker);
      const profitOrLoss = netSaleInLuno - bankFeesParam - withdrawalAmount - amountParam;
      return {coin: pair.coin, profitOrLoss};
    });
    actions.setStorage({ bankFees: bankFeesParam, amount: amountParam, exchangeRate: exchangeRateParam })
    setProfitOrLoss(results);
  };

  if (state.loading) {
    return (
      <>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView style={styles.loadingView}>
          <ActivityIndicator size="large" color="#AC3B61" />
        </SafeAreaView>
      </>
    );
  }

  if(!state.loading && state.error.length > 0) {
    return (
      <>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView style={styles.loadingView}>
          <View style={styles.formView}>
            <Text style={{ paddingBottom: 10 }}>Oops we couldn't retrieve the crpyto pairs, please try again later</Text>
            <Button onPress={() => actions.getPairs()}title={"Retry"}></Button>
          </View>
        </SafeAreaView>
      </>
    );
  }

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.scrollView}>
        <View style={{ ...styles.formView, marginTop: 20 }}>
          <View style={styles.formTextContainer}>
            <Text style={{fontWeight: 'bold', display: 'flex'}}>Amount</Text>
            <TextInput
              style={styles.formInput}
              underlineColorAndroid={"#123C69"}
              placeholder={'R Amount'}
              placeholderTextColor={'#AC3B61'}
              defaultValue={state.cache.amount}
              editable
              onChangeText={e => setAmount(e)}></TextInput>
          </View>
          <View style={styles.formTextContainer}>
            <Text style={{fontWeight: 'bold', display: 'flex'}}>
              Exchange Rate
            </Text>
            <TextInput
              style={styles.formInput}
              underlineColorAndroid={"#123C69"}
              placeholder={'EUR/ZAR Exchange Rate'}
              placeholderTextColor={'#AC3B61'}
              defaultValue={state.cache.exchangeRate}
              editable
              onChangeText={e => setExchangeRate(e)}></TextInput>
          </View>
          <View style={styles.formTextContainer}>
            <Text style={{fontWeight: 'bold', display: 'flex'}}>
              Bank Transfer Fees
            </Text>
            <TextInput
              style={{...styles.formInput, marginBottom: 10}}
              underlineColorAndroid={"#123C69"}
              placeholder={'Fees'}
              placeholderTextColor={'#AC3B61'}
              defaultValue={state.cache.bankFees}
              editable
              onChangeText={e => setBankFees(e)}></TextInput>
          </View>
          <Button
            style={styles.calculateButton}
            title={'Calculate'}
            color={'#123C69'}
            onPress={(e) => calculateArbitrage()}></Button>
        </View>
        <View style={styles.formView}>
          {state.data.kraken.map((pairs, index) => (
            <View style={styles.cryptoPairs}  key={"Kraken" + index}>
              <Text style={{fontWeight: 'bold', paddingRight: 10}}>
                {pairs.title}
              </Text>
              <Text>{pairs.ask}</Text>
            </View>
          ))}
          {state.data.luno.map((pairs, index) => (
            <View style={styles.cryptoPairs}  key={"Luno" + index}>
              <Text style={{fontWeight: 'bold', paddingRight: 10}}>
                {pairs.title}
              </Text>
              <Text>{pairs.bid}</Text>
            </View>
          ))}
        </View>
        {profitOrLoss.length > 0 && (
          <View style={styles.formView}>
            <Text style={{fontWeight: "bold", fontSize: 17, paddingBottom: 10}}>{"Profit / Loss"}</Text>
            {profitOrLoss.map((x, index) => (
              <View style={styles.cryptoPairs} key={"Profit" + index}>
                <Text style={{fontWeight: 'bold', paddingRight: 10}}>
                  {x.coin}
                </Text>
                <Text>R {x.profitOrLoss.toFixed(2)}</Text>
              </View>
            ))}
          </View>
        )}
        { <View style={styles.retryButton}>
            <Button
              title={'Retry'}
              color={'#123C69'}
              onPress={(e) => retry()}></Button>
          </View>
        }
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  retryButton: {
    display: 'flex',
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: '#123C69',
    flexDirection: 'column',
    // borderRadius: 30,
  },
  calculateButton: {
    // display: "flex",
    backgroundColor: '#123C69',
    // marginTop: 20
  },
  formTextContainer: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
  },
  cryptoPairs: {
    display: 'flex',
    flexDirection: 'row',
  },
  crpytoPairsLabel: {},
  formInput: {
    // width: 200,

  },
  formView: {
    display: 'flex',
    // alignItems: "center",
    padding: 15,
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,
    marginBottom: 10,
    borderRadius: 15,

    backgroundColor: '#BAB2B5',
    flexDirection: 'column',
  },
  loadingView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#123C69',
  },
  scrollView: {
    flex: 1,
    backgroundColor: '#EEE2DC',
    flexDirection: 'column',
  },
});

export default App;
