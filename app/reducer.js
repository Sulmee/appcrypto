import _ from 'lodash';
import { LOADING, SUCCESS, FAILURE, CACHE } from "./constants";

export const intialState = {
    loading: true,
    data: {},
    error: "",
    cache: {}
}

export default (state, action) => {
    switch (action.type) {
        case LOADING: {
            return { ...state, loading: true }
        }
        case SUCCESS: {
            const response = action.payload.data
            if( _.filter(response, x => _.isEmpty(x) == true).length > 0 ){
                return { ...state, loading: false, data: {}, error: "Failed to retrieve cryto pairs" }    
            } 
            const krakenBTC = { coin: "BTC", title: "Kraken BTCEUR", ask: parseFloat(response.krakenBTC.result.XXBTZEUR.a[0]), bid: parseFloat(response.krakenBTC.result.XXBTZEUR.b[0])} 
            const krakenETH = { coin: "ETH", title: "Kraken ETHEUR", ask: parseFloat(response.krakenETH.result.XETHZEUR.a[0]), bid: parseFloat(response.krakenETH.result.XETHZEUR.b[0])}
            const krakenXRP = { coin: "XRP", title: "Kraken XRPEUR", ask: parseFloat(response.krakenXRP.result.XXRPZEUR.a[0]), bid: parseFloat(response.krakenXRP.result.XXRPZEUR.b[0])}
            
            const lunoBTC = { coin: "BTC", title: "Luno BTCZAR", ask: parseFloat(response.lunoBTC.ask), bid: parseFloat(response.lunoBTC.bid)}
            const lunoETH = { coin: "ETH",title: "Luno ETHZAR", ask: parseFloat(response.lunoETH.ask), bid: parseFloat(response.lunoETH.bid)}
            const lunoXRP = { coin: "XRP",title: "Luno XRPZAR", ask: parseFloat(response.lunoXRP.ask), bid: parseFloat(response.lunoXRP.bid)}

            return { ...state, loading: false, data: { kraken: [ krakenBTC, krakenETH, krakenXRP], luno: [lunoBTC, lunoETH, lunoXRP ]} }
        }
        case FAILURE: {
            return { ...state, loading: false, error: "Something bad happened" }
        }
        case CACHE: {
            return { ...state, cache: action.payload.data}
        }
        default: {
            return state;
        }

    }
}