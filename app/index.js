import Container from './components/container';
import { StoreProvider } from './store';

export default StoreProvider(Container); 
