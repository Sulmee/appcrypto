    Run instructions for iOS:
```
    • cd /home/meezaan/Projects/AppCrypto && react-native run-ios
    - or -
    • Open AppCrypto/ios/AppCrypto.xcodeproj in Xcode or run "xed -b ios"
    • Hit the Run button
```
    Run instructions for Android:
```
    • Have an Android emulator running (quickest way to get started), or a device connected.
    • cd /home/meezaan/Projects/AppCrypto && react-native run-android
```